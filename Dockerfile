FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y install git swig cmake doxygen \
    build-essential libboost-all-dev libtool libusb-1.0-0 libusb-1.0-0-dev \
    libudev-dev libncurses5-dev libfftw3-bin libfftw3-dev libfftw3-doc \
    libcppunit-1.14-0 libcppunit-dev libcppunit-doc ncurses-bin cpufrequtils \
    python-numpy python-numpy-doc python-numpy-dbg python-scipy \
    python-docutils qt4-bin-dbg qt4-default qt4-doc libqt4-dev libqt4-dev-bin \
    python-qt4 python-qt4-dbg python-qt4-dev python-qt4-doc libqwt6abi1 \
    libncurses5 libncurses5-dbg libfontconfig1-dev libxrender-dev libpulse-dev \
    g++ automake autoconf python-dev libusb-dev fort77 libsdl1.2-dev \
    python-wxgtk3.0 ccache python-opengl libgsl-dev python-cheetah python-mako \
    python-lxml qt4-dev-tools libqwtplot3d-qt5-dev pyqt4-dev-tools \
    python-qwt5-qt4 wget libxi-dev gtk2-engines-pixbuf r-base-dev python-tk \
    liborc-0.4-0 liborc-0.4-dev libasound2-dev python-gtk2 libzmq3-dev libzmq5 \
    python-requests python-sphinx libcomedi-dev python-zmq libqwt-dev \
    python-six libgps-dev libgps23 gpsd gpsd-clients python-gps \
    python-setuptools curl python-bitarray libcanberra-gtk-module autotools-dev

ENV WORK_DIR=/root INSTALL_DIR=/opt
WORKDIR ${WORK_DIR}

ENV GNU_TAG=v3.7.13.4
RUN git clone --recursive https://github.com/gnuradio/gnuradio \
    && cd gnuradio \
    && git checkout $GNU_TAG \
    && git submodule update --init --recursive \
    && mkdir build \
    && cd build \
    && cmake ../ \
    && make \
    && make install \
    && ldconfig \
    && echo 'PYTHONPATH=/usr/local/lib/python2.7/dist-packages' >> /etc/environment

ENV UHD_TAG=v3.14.0.0
RUN git clone https://github.com/EttusResearch/uhd \
    && cd uhd \
    && git checkout $UHD_TAG \
    && cd host \
    && mkdir build \
    && cd build \
    && cmake ../ \
    && make \
    && make test \
    && make install \
    && ldconfig \
    && echo 'LD_LIBRARY_PATH=/usr/local/lib' >> /etc/environment

RUN uhd_images_downloader
RUN cd $WORK_DIR/uhd/host/utils \
    && cp uhd-usrp.rules /etc/udev/rules.d/ \
    && groupadd usrp \
    && usermod -aG usrp root \
    && echo '@usrp - rtprio 99' >> /etc/security/limits.conf
